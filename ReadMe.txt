SoftEther VPN Server, Client for MacOS

Run `sh setup.sh` and then follow the instructions below:
- sudo ./vpnclient start
- sudo ./vpncmd (Select option 2)
- (Press enter)
- NicCreate ethVPN0
- AccountCreate account0
- (Enter IP of VPN Server)
- (Enter HUB)
- (Enter username created in HUB)
- account0
- AccountPassword account0
- (Enter password of username created in HUB)
- standard
- AccountConnect account0
- exit

Modify `connect.sh`.
After modification run `sh connect.sh` to start using vpnclient.

Note:
    Stop command: Run `sudo ./vpnclient stop`
